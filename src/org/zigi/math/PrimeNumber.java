package org.zigi.math;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

public class PrimeNumber {

	private static List<Long> primeNums = null;
	private static Logger log = Logger.getLogger(PrimeNumber.class);
	public static final String PRIMES_FILE = "primes.o";
	private static final List<Long> DEFAULT_PRIMES = new LinkedList<Long>(
			Arrays.asList((long) 2, (long) 3, (long) 5));

	static {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				savePrimes(PRIMES_FILE);
			}
		}));
	}

	public static void main(String[] args) {

		long maxNum = Long.parseLong("123456789101112");
		long start = System.nanoTime();
		System.out.println(isPrime(maxNum));
		long end = System.nanoTime() - start;
		System.out.println("Proběhlo za " + end / 1000000.0 + " ms");
	}

	public static boolean isPrime(long number) {
		if (primeNums == null) {
			loadPrimes(PRIMES_FILE);
		}

		// nemáme dostatek vygenerovaných prvočísel
		if (primeNums.get(primeNums.size() - 1) < Math.floor(Math.sqrt(number))) {
			log.info("Nedostatek vygenerovaných prvočísel");
			long toNumber = (long) Math.floor(Math.sqrt(number));
			generate(toNumber);
		}

		long toNumber = (long) Math.floor(Math.sqrt(number));
		int index = 0;
		while (index < primeNums.size() && primeNums.get(index) <= toNumber) {
			if (number % primeNums.get(index) == 0)
				return false;
			index++;
		}
		return true;
	}

	private static void generate(long moreThan) {
		log.info("Generuji prvočísla do čísla většího než " + moreThan);
		// Generování nových prvočísel
		long from = primeNums.get(primeNums.size() - 1) + 1;
		while (true) {
			if (isPrime(from)) {
				// log.info("Vkládám nové prvočíslo " + from);
				primeNums.add(from);
				if (from > moreThan)
					break;
			}
			from++;
		}
		// zápis do souboru
		savePrimes(PRIMES_FILE);
	}

	private static void savePrimes(String path) {
		log.info("Zapisuji prvočísla (" + primeNums.size() + ")");
		File file = new File(path);
		if (!file.exists()) {
			log.error(String.format(
					"Seznam prvočísel se nepodařilo do souboru (%s) uložit",
					path));
			return;
		}

		try {
			ObjectOutputStream oos = new ObjectOutputStream(
					new BufferedOutputStream(new FileOutputStream(file)));
			// read number of primes
			int size = primeNums.size();
			if (size == 0) {
				primeNums = DEFAULT_PRIMES;
			}
			oos.writeInt(size);
			for (Long i : primeNums) {
				oos.writeLong(i);
			}

			oos.close();
		} catch (FileNotFoundException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}
	}

	private static void loadPrimes(String path) {
		log.info("Načítám prvočísla");
		File file = new File(path);
		if (!file.exists()) {
			try {
				log.warn(String
						.format("Seznam prvočísel se nepodařilo ze souboru (%s) načíst. Vytvářím soubor nový",
								path));
				file.createNewFile();
			} catch (IOException e) {
				log.error(String.format("Soubor (%s) se nepodařilo vytvořit",
						path));
				return;
			}
		}

		int size = 0;
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
					file));

			// read number of primes
			size = ois.readInt();
			if (size > 0) {
				log.info("Načteno " + size + " prvočísel");
				primeNums = new LinkedList<Long>();

				for (int i = 0; i < size; i++) {
					long cislo = ois.readLong();
					// log.info("Načítám nové prvočíslo " + cislo +
					// " ze souboru");
					primeNums.add(cislo);
				}
			} else {
				log.info("Používám defaultní prvočísla");
				primeNums = DEFAULT_PRIMES;
			}
			ois.close();
		} catch (FileNotFoundException e) {
			log.error(e);
		} catch (IOException e) {
			if (size != 0)
				log.error(e);
			else {
				primeNums = DEFAULT_PRIMES;
			}
		}
	}
}
