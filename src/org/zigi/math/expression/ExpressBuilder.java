package org.zigi.math.expression;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import net.objecthunter.exp4j.ExpressionBuilder;

import org.zigi.math.MathVariable;

public class ExpressBuilder {

	public static final List<String> notVariable = new LinkedList<String>();
	static {
		notVariable.add("abs");
		notVariable.add("acos");
		notVariable.add("asin");
		notVariable.add("atan");
		notVariable.add("cbrt");
		notVariable.add("ceil");
		notVariable.add("cos");
		notVariable.add("cosh");
		notVariable.add("exp");
		notVariable.add("floor");
		notVariable.add("log");
		notVariable.add("sin");
		notVariable.add("sinh");
		notVariable.add("sqrt");
		notVariable.add("tan");
		notVariable.add("tanh");
	}

	public static ExpressionContainer createExpression(File file)
			throws IOException {
		if (!file.exists())
			throw new FileNotFoundException();

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(file)));
		String exprString = reader.readLine();
		reader.close();

		return createExpression(exprString);
	}

	public static ExpressionContainer createExpression(String expression) {
		try {
			ExpressionContainer container = new ExpressionContainer();
			List<String> result = MathVariable.parseVariables(expression,
					notVariable);
			String[] vars = new String[result.size()];
			String[] variables = result.toArray(vars);
			container.setExpression(new ExpressionBuilder(expression)
					.variables(vars).build());
			container.setVariables(variables);
			return container;
		} catch (Exception exc) {

		}
		return null;
	}
}
