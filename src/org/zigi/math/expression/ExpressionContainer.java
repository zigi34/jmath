package org.zigi.math.expression;

import net.objecthunter.exp4j.Expression;

public class ExpressionContainer {

	private Expression expression;
	private String[] variables;
	public Expression getExpression() {
		return expression;
	}
	public void setExpression(Expression expression) {
		this.expression = expression;
	}
	public String[] getVariables() {
		return variables;
	}
	public void setVariables(String[] variables) {
		this.variables = variables;
	}
}
