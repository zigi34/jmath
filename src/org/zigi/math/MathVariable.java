package org.zigi.math;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MathVariable {

	public static List<String> parseVariables(String expression)
	{
		List<String> variables = new LinkedList<String>();
		Pattern p = Pattern.compile("[A-Za-z]\\w*");
		Matcher m = p.matcher(expression);
		while(m.find())
		{
			String text = m.group().trim();
			if(!variables.contains(text))
				variables.add(text);
		}
		return variables;
	}
	public static List<String> parseVariables(String expression, List<String> notContain)
	{
		List<String> result = new LinkedList<String>();
		List<String> variables = parseVariables(expression);
		for(String item : variables)
		{
			if(!notContain.contains(item))
				result.add(item);
		}
		return result;
	}
}
