package org.zigi.math;

import java.util.LinkedList;
import java.util.List;

public class Grupa {

	private int grupa_size;
	private List<Integer> grupa;

	public Grupa(int grupa) {
		this.grupa_size = grupa;
		this.grupa = createGrupe(grupa);
	}

	public List<Integer> numbers() {
		return grupa;
	}

	/**
	 * Vytvoření grupy
	 * 
	 * @param cislo
	 *            řád grupy
	 * @return
	 */
	private static List<Integer> createGrupe(int cislo) {
		List<Integer> list = new LinkedList<Integer>();
		for (int i = 1; i <= cislo; i++) {
			if (Utils.gcd(cislo, i) == 1)
				list.add(i);
		}
		return list;
	}

	public List<Integer> rady() {
		List<Integer> rady = new LinkedList<Integer>();
		for (Integer num : grupa) {
			boolean isOver = false;
			for (int i = 1; i < grupa_size + 1; i++) {
				if ((Utils.modulMocnina(num, i, grupa_size)) == 1) {
					rady.add(i);
					isOver = true;
				}
				if (isOver)
					break;
			}
			if (!isOver)
				rady.add(-1);
		}
		return rady;
	}

	public List<Integer> generators() {
		List<Integer> rady = rady();
		List<Integer> list = new LinkedList<Integer>();
		int fi = Utils.eulerNumber(grupa_size);
		for (int i = 0; i < rady.size(); i++) {
			if (fi == rady.get(i))
				list.add(grupa.get(i));
		}
		return list;
	}
}
