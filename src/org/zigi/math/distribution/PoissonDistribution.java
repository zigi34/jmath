package org.zigi.math.distribution;

public class PoissonDistribution {

	public static int getPoisson(double c) {
		// reference to http://introcs.cs.princeton.edu/98simulation/
		double t = 0.0;
		for (int x = 0; true; x++) {
			t = t - Math.log(Math.random()) / c; // sum exponential deviates
			if (t > 1.0)
				return x;
		}
	}
}
