package org.zigi.math;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Utils {
	public static List<Integer> faktorizuj(int n) {
		List<Integer> list = new LinkedList<Integer>();
		int zbytek = n;
		for (int i = 2; i <= Math.sqrt(zbytek); i++) {
			while (zbytek % i == 0) {
				zbytek = zbytek / i;
				list.add(i);
			}
		}

		if (zbytek > 1) {
			list.add(zbytek);
		}
		return list;
	}

	public static int eulerNumber(int cislo) {
		List<Integer> factorizeNumber = faktorizuj(cislo);
		int fi = 1;
		for (Integer num : factorizeNumber)
			fi *= (num - 1);
		return fi;
	}

	/**
	 * Nejmenší společný dělitel čísel a a b
	 * 
	 * @param a
	 *            první číslo
	 * @param b
	 *            druhé číslo
	 * @return
	 */
	public static int gcd(int a, int b) {
		if (a < 1 || b < 1)
			throw new IllegalArgumentException("a or b is less than 1");
		while (b != 0) {
			int tmp = a;
			a = b;
			b = tmp % b;
		}
		return a;
	}

	/**
	 * Nejmenší společný násobek
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static int lcm(int a, int b) {
		if (a == 0 || b == 0) {
			return 0;
		}
		return (a * b) / gcd(a, b);
	}

	/**
	 * Převod dekadického čísla na binární
	 * 
	 * @param number
	 *            dekadické číslo
	 * @return binární reprezentace čísla
	 */
	public static List<Integer> toBinary(int number) {
		List<Integer> list = new LinkedList<Integer>();
		int num = number;
		while (num > 0) {
			list.add(num & 0x1);
			num >>= 1;
		}
		Collections.reverse(list);
		return list;
	}

	public static int modulMocnina(int a, int k, int n) {
		List<Integer> kArray = Utils.toBinary(k);
		Collections.reverse(kArray);
		int b = 1;
		if (k == 0)
			return b;
		int A = a;
		if (kArray.get(0) == 1)
			b = a;
		for (int i = 1; i < kArray.size(); i++) {
			A = (A * A) % n;
			if (kArray.get(i) == 1)
				b = (A * b) % n;
		}
		return b;
	}
}
