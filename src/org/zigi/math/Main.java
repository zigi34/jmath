package org.zigi.math;

import java.util.List;

public class Main {

	public static void main(String[] args) {
		for (int i = 1; i < 761; i++)
			if (PrimeNumber.isPrime(i))
				System.out.print(i + ",");
		System.out.println();
		int cislo = 760;
		StringBuilder sb = new StringBuilder();
		for (Integer num : Utils.toBinary(cislo)) {
			sb.append(num);
		}
		System.out.println(cislo + "(10) = " + sb.toString());
		System.out.println("88^7 mod 187 = " + Utils.modulMocnina(88, 7, 187));
		System.out.println(Utils.eulerNumber(Utils.eulerNumber(761)));
		Grupa g = new Grupa(761);
		for (Integer num : g.numbers()) {
			System.out.print(num + ",");
		}
		System.out.println();
		for (Integer num : g.rady()) {
			System.out.print(num + ",");
		}
		System.out.println();
		for (Integer num : g.generators()) {
			System.out.print(num + ",");
		}
		System.out.println();
		System.out.println(Utils.gcd(175, 89));

		List<Integer> fakt = Utils.faktorizuj(3599);
		for (int i = 0; i < fakt.size(); i++)
			System.out.println(fakt.get(i));
		System.out.println("Fi=" + Utils.eulerNumber(3599));
		for (int i = 1; i < 3480; i++)
			if ((31 * i) % 3480 == 1)
				System.out.println(i);
	}
}
